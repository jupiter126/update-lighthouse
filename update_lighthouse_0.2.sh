#!/bin/bash
#############
# Simple script to update lighthouse prebuilt binaries.
#############
# MIT LICENCE
# Copyright 2020 jupiter126@gmail.com
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# - The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#############
# If you like it, fell free to send some ether to 0xB1DcBDe40202b4d5BD352041126Ba6f29f7f4b77
#############
#
#
# !!!!  DEV VERSION : UNTESTED  !!!!
#
#
############# set variables
ccron="0" # if ccron=1, removes all "echo" to avoid filling logs
bnsysdname="lighthousebeacon" # Systemd name of your lighthouse beacon service
vtsysdname="lighthousevalidator" # Systemd name of your lighthouse beacon service
############
# choose a method, and set parameters in if section
mmethod="prebuilt" # use prebuilt binaries by default - no system requirements (you can switch to build if you want to build)
# prebuilt options:
## Uncomment the appropriate line for your architecture:
#localversion=aarch64-unknown-linux-gnu
#localversion=aarch64-unknown-linux-gnu-portable
#localversion=x86_64-unknown-linux-gnu
localversion=x86_64-unknown-linux-gnu-portable

#build options
builduser="mybuilduser"
builddir="/home/mybuilduser/lighthouse/"
#export RUST_BACKTRACE=full  # uncomment to debug lighthouse binary
############# END of variables
def=$(tput sgr0);bol=$(tput bold);red=$(tput setaf 1;tput bold);gre=$(tput setaf 2;tput bold);yel=$(tput setaf 3;tput bold);blu=$(tput setaf 4;tput bold);mag=$(tput setaf 5;tput bold);cya=$(tput setaf 6;tput bold)
####### Code Start
# Security check
if [[ -f /usr/local/bin/lighthouse ]]; then
	if [[ -f /root/master/lighthouse.checksum.txt ]]; then
		if [[ "$(/usr/bin/sha512sum /usr/local/bin/lighthouse|cut -f1 -d' ')" != "$(cat /root/master/lighthouse.checksum.txt)" ]]; then
			echo "$red !!! WARNING: Checksums don't match: either you did not update as should or you might have been hacked !!! $def"
		else
			if [[ "$ccron" = "0" ]]; then echo "$gre Security Checksum test passed $def"; fi
		fi
	else
		echo "$red !!! WARNING: No checksum file, either you didn't secure lighthouse with this script, or you might have been hacked !!! $def"
	fi
else
	echo "$red !!! WARNING: Lighthouse not detected !!! $def"
fi

function f_versioncheck { #sets $currentver and $latestver
if [[ "$mmethod" = "prebuilt" ]]; then
	if [[ -f /usr/local/bin/lighthouse ]]; then
		currentver="$(/usr/local/bin/lighthouse --version|head -n1|cut -f 2 -d' '|cut -f1 -d'-')"
	else
		currentver="0.0.0"
	fi
	currentver="$(/usr/local/bin/lighthouse --version|head -n1|cut -f 2 -d' '|cut -f1 -d'-')"
	latestver=$(/usr/bin/curl -s https://github.com/sigp/lighthouse/releases/latest|cut -f2 -d'"'|rev|cut -f1 -d'/'|rev)
elif [[ "$mmethod" = "build" ]]; then
	sudo -u $builduser -- bash -c 'cd "$builddir";git fetch --tags;tag=$(git describe --tags `git rev-list --tags --max-count=1`)'
	echo "$tag"
	currentver="notsurehowtogetthisyet"
	latestver="$tag"
fi
}

function f_switch_method {
if [[ "$mmethod" = "prebuilt" ]]; then
	mmethod="build"
elif [[ "$mmethod" = "build" ]]; then
	mmethod="prebuilt"
fi
}

function f_prepare_update {
if [[ "$mmethod" = "prebuilt" ]]; then
		if $(/usr/bin/wget -q "https://github.com/sigp/lighthouse/releases/download/$latestver/lighthouse-$latestver-$localversion.tar.gz"); then
			/usr/bin/tar -xzf "lighthouse-$latestver-$localversion.tar.gz" && rm "lighthouse-$latestver-$localversion.tar.gz" && return 0
		else
			if [[ "$ccron" = "0" ]]; then echo "new version detected but not available as prebuilt yet."; fi && return 1
		fi
elif [[ "$mmethod" = "build" ]]; then
	echo "cd somewhere and make..."
fi
}

function f_apply_update {
f_losenrights
if [[ "$mmethod" = "prebuilt" ]]; then
	mv lighthouse /usr/local/bin/
elif [[ "$mmethod" = "build" ]]; then
	echo "cd somewhere and make..."
fi
}

function f_stop_services { #stop vt befor bn
	for thisservice in $vtsysdname $bnsysdname; do
		/usr/bin/systemctl stop $thisservice
		until [[ "$(systemctl is-active $thisservice)" = "inactive" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $thisservive to stop. . ."; fi
		done
	done
	if [[ "$ccron" = "0" ]]; then echo "$gre $vtsysname and $bnsysdname have been$yel stopped! $def"; fi
}

function f_start_services { #start bn before vt
	for thisservice in $bnsysdname $vtsysdname; do
		/usr/bin/systemctl start $thisservice
		until [[ "$(systemctl is-active $thisservice)" = "active" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $thisservive to start. . ."; fi
		done
	done
	if [[ "$ccron" = "0" ]]; then echo "$gre $bnsysdname and $vtsysname have been$yel  started $def"; fi
}

function f_losenrights {
	chattr -i /usr/local/bin/lighthouse
}

function f_setsecrights {
	chown root:root /usr/local/bin/lighthouse
	chmod 755 /usr/local/bin/lighthouse
	chattr +i /usr/local/bin/lighthouse
	/usr/bin/sha512sum /usr/local/bin/lighthouse|cut -f1 -d' ' > /root/master/lighthouse.checksum.txt
}

function f_autoprebuilt {
	f_versioncheck
	if [[ "x$currentver" != "x$latestver" ]]; then
		if [[ "$ccron" = "0" ]]; then echo "$red Requires update ( $currentver != $latestver ) $def"; fi
		f_prepare_update
		if [[ "$?" != "0" ]]; then exit; fi
		f_stop_services
		f_apply_update
		f_setsecrights
		f_start_services
	else
		if [[ "$ccron" = "0" ]]; then echo "$gre up to date ( $currentver = $latestver ) $def"; fi
	fi
}

function m_main { # Main Menu (displayed if called without args)
while [ 1 ]
do
	echo "$gre current method is $mmethod $def"
	PS3='Choose a number: '
	select choix in "Switch method" "Prepare update" "Stop services" "Apply update" "Set secure rights" "Start services " "Quit"
	do
		echo " ";echo "####################################";echo " "
		break
	done
	case $choix in
		"Switch method") 	f_switch_method ;;
		"Prepare update")	f_prepare_update ;;
		"Stop services")	f_stop_services ;;
		"Apply update")		f_apply_update ;;
		"Set Secure rights")	f_setsecrights ;;
		"Start services")	f_start_services ;;
		Quit)		echo "bye ;)";exit ;;
		*)		echo "$mag Same player shoot again!$def" ;;
	esac
done
}

#Main entry point: Displays menu if no arguments passed, displays help if wrong argument passed
if [[ "x$1" = "x" ]]; then
	m_main
elif [[ "x$1" = "xautoprebuilt" ]]; then
	f_autoprebuilt
else
	echo "$yel Starts menu if launched without parameters, else $yel"
	echo "$blu Optionnal parameters are$yel
	- $red autoprebuilt$yel: auto-update prebuilt binaries $def"
fi
