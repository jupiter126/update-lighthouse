#!/bin/bash
#############
# Simple script to update lighthouse prebuilt binaries.
#############
# MIT LICENCE
# Copyright 2020 jupiter126@gmail.com
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# - The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#############
# If you like it, fell free to send some ether to 0xB1DcBDe40202b4d5BD352041126Ba6f29f7f4b77
#############
cconfig="/root/master/config.txt"
#############
def=$(tput sgr0);bol=$(tput bold);red=$(tput setaf 1;tput bold);gre=$(tput setaf 2;tput bold);yel=$(tput setaf 3;tput bold);blu=$(tput setaf 4;tput bold);mag=$(tput setaf 5;tput bold);cya=$(tput setaf 6;tput bold)

# FOR CRON USE, UPDATE THIS LINE TO MATCH INSTALL DIR ! ! !
if [[ -f "$cconfig" ]]; then
	source "$cconfig"
else
	echo "$red please copy$yel config.txt.sample$red as$yel config.txt$red and edit it to fit your needs$def"; exit
fi

####### Code Start
function f_debug { 		# Defined and called in each function, can be set on 1 in settings.  if called with $1 = status, outputs script variables state at that point.
	if [ "$debug" = "1" ]; then
	        echo "$yel ## debug: $fonction$def"
		if [[ "$1" = "status" ]]; then
			SCRIPT_VARS="`grep -vFe "$VARS" <<<"$(set -o posix ; set)" | grep -v ^VARS=`"; echo "$red$SCRIPT_VARS$def"
		fi
	fi
}

function f_security_check {
	fonction="f_security_check" && f_debug
# Security check
	if [[ -f /usr/local/bin/lighthouse ]]; then
		if [[ -f /root/master/lighthouse.checksum.txt ]]; then
			if [[ "$(/usr/bin/sha512sum /usr/local/bin/lighthouse|cut -f1 -d' ')" != "$(cat /root/master/lighthouse.checksum.txt)" ]]; then
				echo "$red !!! WARNING: Checksums don't match: either you did not update as should or you might have been hacked !!! $def"
			else
				if [[ "$ccron" = "0" ]]; then echo "$gre Security Checksum test passed $def"; fi
			fi
		else
			echo "$red !!! WARNING: No checksum file, either you didn't secure lighthouse with this script, or you might have been hacked !!! $def"
		fi
	else
		echo "$red !!! WARNING: Lighthouse not detected !!! $def"
	fi
	if [[ -d  /var/lib/lighthouse/beacon/ ]]; then
		if [[ "$(stat -c "%a %U %G" /var/lib/lighthouse/beacon/)" != "700 $beaconuser $beaconuser" ]]; then echo "$red /var/lib/lighthouse/beacon/ has insecure ownership/rights $def"; fi
	fi
	if [[ -d  /var/lib/lighthouse/validators/ ]]; then
		if [[ "$(stat -c "%a %U %G" /var/lib/lighthouse/validators/)" != "700 $validuser $validuser" ]]; then echo "$red !!! /var/lib/lighthouse/validators/ has insecure ownership/rights !!! $def"; fi
	fi
	if [[ "$ddeployed" = "1" ]] && [[ "$ddeployer" = "1" ]];then echo "$red The config file is not long but it is important to set things up well!$def"; exit; fi
}

function f_versioncheck { #sets $currentver and $latestver
	fonction="f_versioncheck" && f_debug
	if [[ -f /usr/local/bin/lighthouse ]]; then
		currentver="$(/usr/local/bin/lighthouse --version|head -n1|cut -f 2 -d' '|cut -f1 -d'-')"
	else
		currentver="0.0.0"
	fi
	if [[ "$mmethod" = "prebuilt" ]]; then
		latestver=$(/usr/bin/curl -s https://github.com/sigp/lighthouse/releases/latest|cut -f2 -d'"'|rev|cut -f1 -d'/'|rev)
	elif [[ "$mmethod" = "build" ]]; then
		latestver=$(sudo -u $builduser -- bash -c 'cd $builddir && git fetch --tags && git describe --tags $(git rev-list --tags --max-count=1)')
	fi
}

function f_switch_method {
	fonction="f_switch_method" && f_debug
	if [[ "$mmethod" = "prebuilt" ]]; then
		mmethod="build"
	elif [[ "$mmethod" = "build" ]]; then
		mmethod="prebuilt"
	fi
}

function f_prepare_update {
	fonction="f_prepare_update" && f_debug
	apt update && apt -y upgrade
	if [[ "$mmethod" = "prebuilt" ]]; then
			if $(/usr/bin/wget -q "https://github.com/sigp/lighthouse/releases/download/$latestver/lighthouse-$latestver-$localversion.tar.gz"); then
				/usr/bin/tar -xzf "lighthouse-$latestver-$localversion.tar.gz" && rm "lighthouse-$latestver-$localversion.tar.gz" && return 0
			else
				if [[ "$ccron" = "0" ]]; then echo "new version detected but not available as prebuilt yet."; fi && return 1
			fi
	elif [[ "$mmethod" = "build" ]]; then
		sudo -u $builduser -- bash -c 'rustup -q update && cd $builddir && git fetch && git checkout stable && make' # -j12 change the number of j accordingly to your CPU
	        /usr/bin/sha512sum /home/$builduser/lighthouse/target/release/lighthouse|cut -f1 -d' ' > /root/lightbuild.checksum.txt

	fi
}

function f_isup { 		# Checks if $hhostname is up, returns 0 if up, 1 if not
	fonction="f_isup" && f_debug
	if [[ "$1" != "" ]]; then hhostname="$1"; fi
	if ! ping -c1 $hhostname > /dev/null 2>&1; then
		return 1  #no
	else
		return 0 #yes
	fi
}

function f_add_sshbridges { # Generates the required to establish an ssh bridge webhosts
	fonction="f_add_sshbridge" && f_debug
	for i in webhost1 webhost2; do
		hhostname="$i"
		sshuser="user_$i"
		sshport="port_$i"
		destfile="file_$i"
		if ! f_isup; then
			echo "Host does not ping: could be down or firewalled. Continue anyway? (Y/N)"
			read answer
			if [[ "$answer" != "Y" ]] && [[ "$answer" != "y" ]]; then
				echo "$red Aborted" && continue
			fi
		fi
		echo "Patience,... generating RSA keys..."
		if [[ ! -d $sshkeydir ]]; then mkdir -p $sshkeydir && chmod 700 $sshkeydir; fi
		if [[ ! -f $sshkeydir/$hhostname-id_rsa ]]; then
			if ! ssh-keygen -t rsa -b 4096 -f $sshkeydir/$hhostname-id_rsa -q -P ""; then echo "ssh-keygen failed"; continue; fi
		fi
		if ssh-copy-id -p $sshport -i "$sshkeydir/$hhostname-id_rsa.pub" $sshuser@$hhostname; then
			echo "ssh-copy-id successful"
		else
			echo "ssh-copy-id failed"
			continue
		fi
		if ssh -p $sshport $sshuser@$hhostname -i keys/$hhostname-id_rsa "exit"; then
			echo "$gre ssh bridge successfully installed with $hhostname$def"
		else
			echo "ssh bridge installation failed... check manually"
		fi
	done
}

function f_deployer {
	fonction="f_deployer" && f_debug
	for i in webhost1 webhost2; do
		hhostname="$i"
		sshuser="user_$i"
		sshport="port_$i"
		destfile="file_$i"
		if [[ "$i" = "webhost1" ]]; then ssource="/home/$builduser/lighthouse/target/release/lighthouse";
		elif [[ "$i" = "webhost2" ]]; then source="/root/lightbuild.checksum.txt";
		fi
		scp -q -i $sshkeydir/$hhostname-id_rsa -P $sshport $ssource $sshuser@$hhostname:$destfile
	done
}

function f_deployed {
	fonction="f_deployer" && f_debug
	for ffile in lighthouse lightsum tempchk; do if [[ -f "$ffile" ]]; then rm "$ffile"; fi; done
	wget -q -O tempchk "$chkfile"
	if [[ "$(md5sum tempchk|cut -f1 -d' ')" != "$(md5sum lightsum|cut -f1 -d' ')"  ]]; then #checksum has changed
		wget -q -O lighthouse "$binfile"
		if [[ "$(/usr/bin/sha512sum lighthouse|cut -f1 -d' ')" != "$(cat lighthsum)" ]]; then
			echo "$red !!! WARNING: Checksums don't match - update failed !!! $def"
			rm tempchk lighthouse
		else
			mv tempchk lightsum
			mv lighthouse /usr/local/bin/
			f_setsecrights
		fi
	fi
}

function f_apply_update {
	fonction="f_apply_update" && f_debug
	f_losenrights
	if [[ "$mmethod" = "prebuilt" ]]; then
		testversion="/root/master/lighthouse"
	elif [[ "$mmethod" = "build" ]]; then
		testversion="/home/$builduser/lighthouse/target/release/lighthouse"
	fi
	if [[ "$($testversion --version|head -n1|cut -f 2 -d' '|cut -f1 -d'-')" = "$latestver" ]]; then
		mv "$testversion" /usr/local/bin/
	fi
}

function f_stop_services { #stop vt befor bn
	fonction="f_stop_services" && f_debug
	for thisservice in $vtsysdname $bnsysdname; do
		/usr/bin/systemctl stop $thisservice
		until [[ "$(systemctl is-active $thisservice)" = "inactive" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $thisservive to stop. . ."; fi
		done
	done
	if [[ "$ccron" = "0" ]]; then echo "$gre $vtsysdname and $bnsysdname have been$yel stopped! $def"; fi
}

function f_start_services { #start bn before vt
	fonction="f_start_services" && f_debug
	for thisservice in $bnsysdname $vtsysdname; do
		/usr/bin/systemctl start $thisservice
		until [[ "$(systemctl is-active $thisservice)" = "active" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $thisservive to start. . ."; fi
		done
	done
	if [[ "$ccron" = "0" ]]; then echo "$gre $bnsysdname and $vtsysdname have been$yel  started $def"; fi
}

function f_losenrights {
	fonction="f_losenrights" && f_debug
	if [[ -f /usr/local/bin/lighthouse ]]; then
		chattr -i /usr/local/bin/lighthouse
	fi
}

function f_setsecrights {
	fonction="f_setsecrights" && f_debug
	chown root:root /usr/local/bin/lighthouse
	chmod 755 /usr/local/bin/lighthouse
	chattr +i /usr/local/bin/lighthouse
	/usr/bin/sha512sum /usr/local/bin/lighthouse|cut -f1 -d' ' > /root/master/lighthouse.checksum.txt
}

function f_autoprebuilt {
	fonction="f_autoprebuilt" && f_debug
	f_versioncheck
	if [[ "x$currentver" != "x$latestver" ]]; then
		if [[ "$ccron" = "0" ]]; then echo "$red Requires update ( $currentver != $latestver ) $def"; fi
		f_prepare_update
		if [[ "$?" != "0" ]]; then exit; fi
		f_stop_services
		f_apply_update
		f_setsecrights
		f_start_services
	else
		if [[ "$ccron" = "0" ]]; then echo "$gre up to date ( $currentver = $latestver ) $def"; fi
	fi
}

function f_setupbuildenv {
	fonction="f_setupbuildenv" && f_debug
	if [[ "$(grep $builduser /etc/passwd)" = "" ]]; then
		useradd -m -p $(openssl passwd -crypt $(head /dev/urandom | tr -dc A-Za-z0-9 | head -c20)) $builduser # create builduser with random pass
		echo "DenyUsers $builduser" >> /etc/ssh/sshd_config # pass is truncated to 8 char, so disable ssh for this user
		systemctl restart sshd
	fi
	apt update && apt -y install git gcc g++ make cmake pkg-config # install build dependencies
	sudo -u $builduser -- bash -c 'cd /home/$builduser && curl --proto =https --tlsv1.2 -sSf https://sh.rustup.rs | sh' # install rust for $builduser
	sudo -u $builduser -- bash -c 'cd /home/$builduser && git clone https://github.com/sigp/lighthouse.git' # clone lighthouse for $builduser
}

function m_main { # Main Menu (displayed if called without args)
while [ 1 ]
do
	echo "$gre current method is $mmethod $def"
	PS3='Choose a number: '
	select choix in "Switch method" "Setup build env" "Prepare update" "Stop services" "Apply update" "Set secure rights" "Start services " "Quit"
	do
		echo " ";echo "####################################";echo " "
		break
	done
	case $choix in
		"Switch method") 	f_switch_method ;;
		"Seup build env")	f_setupbuildenv ;;
		"Prepare update")	f_prepare_update ;;
		"Stop services")	f_stop_services ;;
		"Apply update")		f_apply_update ;;
		"Set secure rights")	f_setsecrights ;;
		"Start services")	f_start_services ;;
		Quit)		echo "bye ;)";exit ;;
		*)		echo "$mag Same player shoot again!$def" ;;
	esac
done
}

#Main entry point: Displays menu if no arguments passed, displays help if wrong argument passed
f_security_check

if [[ "x$1" = "x" ]]; then
	m_main
elif [[ "x$1" = "xautoprebuilt" ]]; then
	f_autoprebuilt
else
	echo "$yel Starts menu if launched without parameters, else $yel"
	echo "$blu Optionnal parameters are$yel
	- $red autoprebuilt$yel: auto-update prebuilt binaries $def"
fi
