#!/bin/bash
#############
# Simple script to update lighthouse prebuilt binaries.
#############
# MIT LICENCE
# Copyright 2020 jupiter126@gmail.com
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# - The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#############
# If you like it, fell free to send some ether to 0xB1DcBDe40202b4d5BD352041126Ba6f29f7f4b77
#############
## Uncomment the appropriate line for your architecture:
#localversion=aarch64-unknown-linux-gnu
#localversion=aarch64-unknown-linux-gnu-portable
#localversion=x86_64-unknown-linux-gnu
localversion=x86_64-unknown-linux-gnu-portable
ccron="0" # if ccron=1, removes all "echo" to avoid filling logs
bnsysdname="lighthousebeacon" # Systemd name of your lighthouse beacon service
vtsysdname="lighthousevalidator" # Systemd name of your lighthouse beacon service
####### Code Start
#export RUST_BACKTRACE=full
if [[ -f /usr/local/bin/lighthouse ]]; then
	currentver="$(/usr/local/bin/lighthouse --version|head -n1|cut -f 2 -d' '|cut -f1 -d'-')"
else
	currentver="0.0.0"
fi

currentver="$(/usr/local/bin/lighthouse --version|head -n1|cut -f 2 -d' '|cut -f1 -d'-')"
latestver=$(/usr/bin/curl -s https://github.com/sigp/lighthouse/releases/latest|cut -f2 -d'"'|rev|cut -f1 -d'/'|rev)

if [[ "x$currentver" != "x$latestver" ]]; then
	if [[ "$ccron" = "0" ]]; then echo "requires update ( $currentver != $latestver )"; fi
	if $(/usr/bin/wget -q "https://github.com/sigp/lighthouse/releases/download/$latestver/lighthouse-$latestver-$localversion.tar.gz"); then
		/usr/bin/tar -xzf "lighthouse-$latestver-$localversion.tar.gz"
		/usr/bin/systemctl stop $vtsysname
		until [[ "$(systemctl is-active $vtsysname)" = "inactive" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $vtsysname to stop. . ."; fi
		done
		/usr/bin/systemctl stop $bnsysdname
		until [[ "$(systemctl is-active $bnsysdname)" = "inactive" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $bnsysdname to stop. . ."; fi
		done
		chown root:root lighthouse && chmod 755 lighthouse && mv lighthouse /usr/local/bin/
		rm "lighthouse-$latestver-$localversion.tar.gz"
		/usr/bin/systemctl start $bnsysdname
		until [[ "$(systemctl is-active $bnsysdname)" = "active" ]]; do
			sleep 1; if [[ "$ccron" = "0" ]]; then echo "Waiting for $bnsysdname to start. . ."; fi
		done
		/usr/bin/systemctl start $vtsysname
	else
		if [[ "$ccron" = "0" ]]; then echo "new version detected but not available as prebuilt yet."; fi
	fi
else
	if [[ "$ccron" = "0" ]]; then echo "up to date ( $currentver = $latestver )"; fi

fi
